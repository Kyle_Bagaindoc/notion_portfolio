# Kyle Bagaindoc

I am a fresh graduate from University of Perpetual Help System DALTA. I have no experience as of the moment. 

My focus area is front-end development. I have used CSS and HTML during my OJT and college days. One thing about myself that I want to highlight is that I enjoy learning about new things that will enhance my skills.

# Contact Information

LinkedIn: [https://www.linkedin.com/in/bagaindockyle/](https://www.linkedin.com/in/bagaindockyle/)

Website Portfolio: 

# Work Experience

## Flutter Developer Trainee

FFUF, from July 2021 to August 2021

- I am currently training for Flutter.
- I am training to know more about the Flutter and Dart language as well as best coding practices.

# Skills

## Technical Skills

- Programming

## Soft Skills

- Open-Mindedness
- Teamwork
- Communication
- Creativity

# Education

## BS Computer Science

University of Perpetual Help System DALTA, 2015-2020